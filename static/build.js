(function () {
    var mountPoint = document.getElementById('tree');

    function Node(n, c) {
        this.name = n;
        this.children = c || [];
        this.state = 'c';
    }

    Node.prototype.getChild = function (id) {
        return this.children.filter(function (c) {
            return c.name === id;
        })[0];
    };

    Node.prototype.findNode = function (path) {
        var next = path.shift();
        var result = null;
        if (next === this.name) {
            if (!path.length) {
                return this
            } else {
                var child = this.getChild(path[0]);
                if (child) {
                    return child.findNode(path);
                } else {
                    return null;
                }
            }
        }

        return result;
    };

    Node.prototype.toString = function () {
        var cText = this.children.map(function (c) {
            return c.toString();
        }).join('');
        return '<li data-state="' + (this.state === 'e' ? 'expanded' : 'collapsed') + '" data-title="' + this.name + '">' + this.name + (cText ? '<ul>' + cText + '</ul>' : '') + '</li>';
    };

    var tree = new Node('c:\\');

    function drawTree() {
        mountPoint.innerHTML = tree.toString();
    }

    function loadData(dir) {
        $.get('api/' + dir).then(function (data) {
            var node = tree.findNode(dir.split('/'));
            if (node) {
                node.children = data.map(function (c) {
                    return new Node(c);
                });
            }
            drawTree(tree);
        });
    }

    mountPoint.addEventListener('click', function (e) {
        var cur = e.target;
        var path = [];
        while (cur.id !== 'tree') {
            if (cur.tagName === 'LI') {
                path.unshift(cur.getAttribute('data-title'));
            }
            cur = cur.parentNode;
        }

        var node = tree.findNode(path.slice(0));
        node.state = node.state === 'e' ? 'c' : 'e';

        loadData(path.join('/'));
    });

    loadData('c:\\');
}());