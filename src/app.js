var fs = require('fs');
var express = require('express');
var app = express();

app.get('/api/*', function (req, res) {
    var path = req.url.substring(5);
    console.log(req.url);
    fs.readdir(path, function (err, data) {
        if (err) {
            res.status(400);
            return res.send(err);
        }

        var delay = Math.random() * 500 + 500;
        setTimeout(function () {
            res.send(data);
        }, delay);
    })
});

app.use(express.static('static'));
var server = app.listen(3000, function () {
    var host = server.address().address;
    var port = server.address().port;

    console.log('Example app listening at http://%s:%s', host, port);
});